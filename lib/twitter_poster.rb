module TwitterPoster
  extend ActiveSupport::Concern
  def self.included(klazz)
    klazz.extend ClassMethods
  end

  module ClassMethods
  end

  def post_quote_to_twitter!
    $twitter.update(self.quote)
 end  
end