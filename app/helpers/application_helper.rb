module ApplicationHelper

  ALERT_TYPES = [:success, :info, :warning, :danger] unless const_defined?(:ALERT_TYPES)

  def bootstrap_flash(options = {})
    flash_messages = []
    flash.each do |type, message|
      # Skip empty messages, e.g. for devise messages set to nothing in a locale file.
      next if message.blank?

      type = type.to_sym
      type = :success if type == :notice
      type = :danger  if type == :alert
      type = :danger  if type == :error
      next unless ALERT_TYPES.include?(type)

      tag_class = options.extract!(:class)[:class]
      tag_options = {
        class: "alert fade in alert-#{type} #{tag_class}"
      }.merge(options)

      close_button = content_tag(:button, raw("&times;"), class: "close", "data-dismiss" => "alert")

      Array(message).each do |msg|
        text = content_tag(:div, close_button + msg.html_safe, tag_options)
        flash_messages << text if msg
      end
    end
    flash_messages.join("\n").html_safe
  end

  def mood_display(mood)
     case mood.to_i
     when 1
      content_tag "b", fa_icon("smile-o 2x",  class: "text-info")
     when 2
       content_tag "b",  fa_icon("meh-o  2x",  class: "text-inverse")
     when 3             
       content_tag "b",  fa_icon("frown-o  2x", class: "text-danger")
     else
      mood
     end
  end


  def mood_count_display(mood, count)
     content_tag "span",  mood_display(mood) + "(#{count})"
  end

  def mood_select_options
    [
      [fa_icon("smile-o 2x",  text:'Happy', class: "text-info"), 1],
      [fa_icon("meh-o  2x",  text:'Meh', class: "text-inverse"), 2],
      [fa_icon("frown-o  2x",text:'Unhappy', class: "text-danger"), 3]
    ]
  end

  def date_format d
    d.strftime("%F") if d
  end
  alias :df :date_format

  def datetime_format d
    d.strftime("%F %H:%M") if d
  end
  alias :dtf :datetime_format

  #panel and form actions
  def back_link(text, path, styleclass='', options={})
    link_to fa_icon('caret-square-o-left', :text => text) , path, options.merge(:class => "btn btn-default #{styleclass}")
  end

  def home_link(text, path, styleclass='', options={})
    link_to fa_icon('home', :text => text) , path, options.merge(:class => "btn btn-default #{styleclass}")
  end

  def trash_link(text, path, styleclass='', options={})
    link_to fa_icon('trash-o', :text => text), path, options.merge(:method => :delete, :class => "btn btn-warning #{styleclass}", :data => {:confirm => "Are you sure you want to permanently remove this?" })
  end

  def new_link(text, path, styleclass='', options={})
    link_to fa_icon('plus', :text => text) , path, options.merge(:class => "btn btn-success #{styleclass}")
  end

  def edit_link(text, path, styleclass='', options={})
    link_to fa_icon('pencil', :text => text) , path, options.merge(:class => "btn btn-primary #{styleclass}")
  end

  def view_link(text, path, styleclass='', options={})
    link_to fa_icon('eye', :text => text) , path, options.merge(:class => "btn btn-primary #{styleclass}")
  end

    def submit_options(extra={})
    {"class" => "btn  btn-primary btn-lg", 'data-disable-with' => "<i class='fa fa-spinner fa-spin'></i> Please wait..."}.merge(extra)
  end

  def html_submit(content=fa_icon('save', :text => "Save"), extra={})
    content_tag "button", content.html_safe,  submit_options(extra)
  end

  def yesno(value,yes_text="Yes", no_text="No", yes_label_class="label label-success", no_label_class="label label-warning" )
    if value
      content_tag(:span, fa_icon("check-square", text: yes_text), class: yes_label_class )
    else
      content_tag(:span, fa_icon("square-o", text:no_text), class: no_label_class)
    end
  end

end
