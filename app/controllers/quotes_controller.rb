class QuotesController < ApplicationController
  before_action :set_quote, only: [:show, :edit, :update, :destroy, :post_to_twitter]

  # GET /quotes
  def index
    params[:q]||={}
    # ransack misbehaving, temporary fix
    search = ["%", params[:q][:quote_cont_or_author_cont_or_quote_source_cont].to_s, "%"].join
    @quotes = Quote.where("LOWER(quote) LIKE :search OR LOWER(author) LIKE :search OR LOWER(quote_source) LIKE :search", search: search )
    remaining_params = params[:q].except("quote_cont_or_author_cont_or_quote_source_cont")
      
    @quotes = @quotes.search(remaining_params)
    smart_listing_create(:quotes_smart_listing, @quotes.result,
    partial: "quotes/list",
    default_sort: {id: "desc"},
    remote: true
    )
    render :action => "index", :layout => "application"
  end

  # GET /quotes/1
  def show
  end

  # GET /quotes/new
  def new
    @quote = Quote.new
  end

  # GET /quotes/1/edit
  def edit
  end

  # POST /quotes
  def create
    @quote = Quote.new(quote_params)

    if @quote.save
      redirect_to @quote, notice: 'Quote was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /quotes/1
  def update
    if @quote.update(quote_params)
      redirect_to @quote, notice: 'Quote was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /quotes/1
  def destroy
    @quote.destroy
    redirect_to quotes_url, notice: 'Quote was successfully destroyed.'
  end


  def post_to_twitter
    if @quote.posted_to_twitter
      redirect_to :back, alert: 'Quote already posted to twitter.'
    else
      begin
        @quote.post_quote_to_twitter!
        Quote.where(id: @quote.id).update_all(posted_to_twitter: true)        
        redirect_to :back, notice: 'Quote successfully posted to twitter.'
      rescue Exception => e
        redirect_to :back, alert: e.message
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote
      @quote = Quote.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def quote_params
      params.require(:quote).permit(:author, :quote, :mood, :quote_source)
    end
end
