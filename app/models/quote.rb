# == Schema Information
#
# Table name: quotes
#
#  id                :integer          not null, primary key
#  author            :string
#  quote             :text
#  mood              :integer
#  quote_source      :text
#  processed_quote   :text
#  posted_to_twitter :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_quotes_on_mood  (mood)
#

class Quote < ActiveRecord::Base
  
  # lib/twitter_poster
  include  TwitterPoster

  validates :author, :quote, :mood, :quote_source, :presence => true
  validates :mood, :inclusion =>  {:in => [1,2,3]}

  validates :quote, length: { :maximum =>  140}

  before_save :process_quote

  def self.mood_counts
    all.group(:mood).count
  end

  def quote=(v)
    self[:quote] = v.to_s.encode('UTF-8', invalid: :replace, undef: :replace, replace: '')
  end

  private
  def process_quote
    self[:processed_quote] = quote.to_s.first_and_last_char_of_each_word_upcased
    # new or updated
    self[:posted_to_twitter] = false
    return true
  end
end
