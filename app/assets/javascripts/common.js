

function initialize(container){
  container = container ? $(container) : $(document);
  $(container).find("select").addClass('form-control').select2({
    allowClear: true,
    placeholder: "Select..."
  });
}

$(function(){
  initialize();
})