require 'rails_helper'

RSpec.describe String do
  class RandomObject
    attr_accessor :name

    def to_s
      (name.present? ? name : super).first_and_last_char_of_each_word_upcased
    end
  end

  describe "first_and_last_char_upcased" do
    it "Converts first and last character of word to upcase" do
      word = "hello"
      expect(word.first_and_last_char_upcased).to eq("HellO")
    end
  end

  describe "first_and_last_char_of_each_word_upcased" do
    it "Converts each word's first character and last character in a sentence to upcase" do
      sentence = "Converts each word's first character and last character in a sentence to upcase"
      expect(sentence.first_and_last_char_of_each_word_upcased).to eq("ConvertS EacH Word'S FirsT CharacteR AnD LasT CharacteR IN A SentencE TO UpcasE")
    end
  end


  describe "first_and_last_char_of_each_word_upcased" do
    it "Converts each word's first character and last character in a sentence to upcase and        preserves       spaces" do
      sentence = "Converts each word's first character and last character in a sentence to upcase and        preserves       spaces"
      expect(sentence.first_and_last_char_of_each_word_upcased).to eq("ConvertS EacH Word'S FirsT CharacteR AnD LasT CharacteR IN A SentencE TO UpcasE AnD        PreserveS       SpaceS")
    end
  end


  describe "first_and_last_char_of_each_word_upcased in Threads" do
    it "Converts each word's first character and last character in a sentence to upcase in Threaded mode" do
      all_strings  = (1..10).to_a.map{ Faker::Lorem.sentence}
      threads = []
      puts "Converting strings in threads"
      all_strings.each_with_index do |s, index|
        threads << Thread.new { puts  ["index: #{index} => ",  s.first_and_last_char_of_each_word_upcased].join() } 
      end        
      threads.each { |thr| thr.join }
    end
  end

  describe "first_and_last_char_of_each_word_upcased on RandomObject" do
    it "convert first_and_last_char_of_each_word_upcased sucessfully" do
      r = RandomObject.new
      r.name = "john doe"
      expect(r.to_s).to eq("JohN DoE")
    end
  end
end