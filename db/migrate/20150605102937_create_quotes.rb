class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.string :author
      t.text :quote
      t.integer :mood
      t.text :quote_source
      t.text :processed_quote
      t.boolean :posted_to_twitter
      t.timestamps null: false
    end
    add_index :quotes, :mood
    # add_index :quotes, :author
    # add_index :quotes, :quote
    # add_index :quotes, :quote_source
  end
end
