# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Quote.delete_all
puts "Adding 1000 quotes"
1000.times do 
  Quote.create!(author:Faker::Name.name ,quote: Faker::Lorem.sentence[0,140], mood: [1,2,3].sample, quote_source: Faker::Internet.url('quotes.com'))
end
