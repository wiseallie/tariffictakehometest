# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150605102937) do

  create_table "quotes", force: :cascade do |t|
    t.string   "author"
    t.text     "quote"
    t.integer  "mood"
    t.text     "quote_source"
    t.text     "processed_quote"
    t.boolean  "posted_to_twitter"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "quotes", ["mood"], name: "index_quotes_on_mood"

end
