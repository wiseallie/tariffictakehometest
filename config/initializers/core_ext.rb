class String
  # esure that the string maintains the spaces in their give order.
  def first_and_last_char_of_each_word_upcased
    return self.gsub(/\S+/, &:first_and_last_char_upcased)
  end

  def first_and_last_char_upcased   
    chars = self.split('')
    first_char_upcased =  chars.shift.to_s.upcase
    last_char_upcased = chars.pop.to_s.upcase
    return [first_char_upcased, chars, last_char_upcased].flatten.join
  end
end


