
# Use this setup block to configure all options available in SimpleForm.
SimpleForm.setup do |config|
  config.boolean_style = :nested

  config.wrappers :bootstrap3, tag: 'div', class: 'form-group', error_class: 'has-error',
      defaults: { input_html: { class: 'default_class' } } do |b|

    b.use :html5
    b.use :min_max
    b.use :maxlength
    b.use :placeholder

    b.optional :pattern
    b.optional :readonly

    b.use :label_input
    b.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
    b.use :error, wrap_with: { tag: 'span', class: 'help-block has-error' }
  end

 config.wrappers :bootstrap3_horizontal, tag: 'div', class: 'form-group', error_class: 'has-error',  label_html: {class: 'col-lg-4 col-md-4'}, input_html: { class: "form-control" } do |b|

    b.use :html5
    b.use :min_max
    b.use :maxlength
    b.use :placeholder

    b.optional :pattern
    b.optional :readonly

    b.use :label
    b.wrapper :right_wrapper, tag: :div , class: "col-lg-8 col-md-8" do |component|
      component.use :input
      component.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
      component.use :error, wrap_with: { tag: 'span', class: 'help-block has-error' }
    end
  end

  config.wrappers :group, tag: 'div', class: "form-group", error_class: 'has-error',  label_html: {class: 'col-lg-4 col-md-4'}, input_html: { class: "form-control" } do |b|

    b.use :html5
    b.use :min_max
    b.use :maxlength
    b.use :placeholder

    b.optional :pattern
    b.optional :readonly

    b.use :label

    b.wrapper :right_wrapper, tag: :div , class: "col-lg-8 col-md-8" do |component|
      component.use :input, wrap_with: { class: "input-group" }
      component.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
      component.use :error, wrap_with: { tag: 'span', class: 'help-block has-error' }
    end


    # b.use :input, wrap_with: { class: "input-group" }
    # b.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
    # b.use :error, wrap_with: { tag: 'span', class: 'help-block has-error' }
  end

  config.wrappers :append, tag: 'div', class: "form-group", error_class: 'has-error',  label_html: {class: 'col-lg-4 col-md-4'}, input_html: { class: "form-control" } do |b|

    b.use :html5
    b.use :min_max
    b.use :maxlength
    b.use :placeholder

    b.optional :pattern
    b.optional :readonly

    b.use :label

    b.wrapper :right_wrapper, tag: :div , class: "col-lg-8 col-md-8" do |component|
      component.use :input, wrap_with: { class: "input-group" }
      component.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
      component.use :error, wrap_with: { tag: 'span', class: 'help-block has-error' }
    end


    # b.use :input, wrap_with: { class: "input-group" }
    # b.use :hint,  wrap_with: { tag: 'span', class: 'help-block' }
    # b.use :error, wrap_with: { tag: 'span', class: 'help-block has-error' }
  end

  config.default_wrapper = :bootstrap3_horizontal
end